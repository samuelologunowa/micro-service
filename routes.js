const userControllers = require("./controllers/userController");
// const walletControllers = require("./controllers/walletController");
module.exports = (express) =>{
    const userRoute = express.Router();
    userRoute.post("/register",userControllers.createUser);
    userRoute.post("/login",userControllers.userLogin);
    // userRoute.get("/createwallet/:id",walletControllers.createWallet)
    return userRoute;
}