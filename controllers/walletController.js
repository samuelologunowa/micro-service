const walletServiceUrl = `http://localhost:9596/api/v1`;
const axios = require("axios");

const createWallet = async(userUuid)=>{
    console.log(userUuid);
    try {
        const data =  await axios.post(`${walletServiceUrl}/wallets/create`,{userUuid});
        return data;
    }
    catch(err){
        return err;
    }
   
}
module.exports = {
    createWallet
}