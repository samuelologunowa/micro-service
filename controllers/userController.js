"use strict"

const {pool} = require("../db");
const helper = require("../helper");
const wallet = require("./walletController");

const createUser = async(req,res)=>{
    if(!helper.isValidEmail(req.body.email)){
        res.send({status:400,message: "Invalid email format" });
        return false;
    }
    if(!req.body.email||!req.body.password){
        res.send({status:400,message:"email and password required"});
        return false;
    }
    const hashedPwd = helper.hashpassword(req.body.password);
    let insertQuery = `INSERT INTO users(full_name,email,password) VALUES($1,$2,$3) returning *`;

    const values = [
        req.body.fullName,
        req.body.email,
        hashedPwd
    ];
    try {
        const {rows} = await pool.query(insertQuery,values);
        const data = await wallet.createWallet(rows[0].id);
        if(data.response){
            if(!(data.response.data.status)){
                //if wallet creation is not successful remove user,so we dont have users without wallet
                const delQuery = `DELETE FROM users WHERE id = $1`
                const del = await pool.query(delQuery,[rows[0].id]);
                return res.send(data.response.data);
            }
        }
        else{
            return res.send({status:201,message:"User registration successful",data:rows[0]});
        }        
    } catch (error) {
        if(error.code == "23505")
          return res.send({status:400,message:"email has been taken"});
        return res.send({status:400,message:error});
    }    
}

// const getFullName = async (userId)=>{
//     const query =`SELECT full_name FROM users WHERE id = $1`;
//     try{
//         const {rows} = await pool.query(query,[userId]);
//         console.log("got here",rows[0]);
//         return rows[0].full_name;
//     }
//     catch(error){
//         console.log("error here",err)
//         return error;
//     }
//     const data = await pool.query(query,[userId]);
//     return userId;
    
// }

const userLogin = async(req,res)=>{
    if(!req.body.email||!req.body.password){
        res.send({status:400,message:"email and password required for login"});
        return false;
    }
    if(!helper.isValidEmail(req.body.email)){
        res.send({status:400,message: "Invalid email" });
        return false;
    }
    const selectQuery = `SELECT * FROM users WHERE email = $1`;
    try{
        const {rows} = await pool.query(selectQuery,[req.body.email]);
        if(!rows[0]){
            res.send({status:400,message:"invalid email or password combination"});
            return false;
        }
        if(!helper.comparePwd(rows[0].password,req.body.password)){
            res.send({status:400,message:"invalid email or password combination"});
            return false;
        }

        const token = helper.genToken(req.body);
        return res.send({status:200,message:"Login successful",data:rows,token:token});
    }
    catch(error){
        res.send({status:400,error:error});
    }
}

module.exports = {
    createUser,
    userLogin,
}