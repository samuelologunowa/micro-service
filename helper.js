const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {secret} = require("./config");
const { pool }= require("./db");

/**
 * returns a hashed password
 * @param {string} password 
 * @returns {string} string
 */
const hashpassword = (password)=>{
    return bcrypt.hashSync(password,bcrypt.genSaltSync(8));
}
/**
 * compare password
 * @param {string} hashpwd 
 * @param {string} pwd
 * @returns {boolean} True or False 
 */
const comparePwd = (hashpwd,pwd)=>{
    return bcrypt.compareSync(pwd,hashpwd);
}
/**
 * Check for email validity
 * @param {string} email 
 * @returns {boolean} True or False
 */
const isValidEmail = (email)=>{
    return /\S+@\S+\.\S+/.test(email);
}

var getFullName = async(userId)=>{
    const query =`SELECT full_name FROM users WHERE id = $1`;
    try{
        const {rows} = await pool.query(query,[userId]);
        return rows[0].full_name;
    }
    catch(error){
        console.log("error here",err)
        return error;
    }
    
}

/**
 * Signs a payload and generates token
 * @param {Object} payload 
 * @returns {string} token
 */
const genToken = (payload)=>{
    const token = jwt.sign(payload,secret,{expiresIn:"3d"});
    return token;
}

module.exports = {
    hashpassword,
    comparePwd,
    isValidEmail,
    genToken,
    getFullName
}