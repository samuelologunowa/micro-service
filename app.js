const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const endpoints = require("./routes")(express);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use("/",endpoints);

module.exports = app ;