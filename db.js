const {Pool} = require("pg");
const pgtools = require("pgtools");
const {db} = require("./config");

const createDb = async()=>{
    const conn = {user:db.user,host:db.host,password:db.password,port:db.port};
    await pgtools.createdb(conn,db.database);
}
const pool = new Pool(db);
pool.on("connect",()=>{
    console.log("connected to postgres db");
})
const createUserTable = async()=>{
    createDb().then(()=>{
        const queryText = `CREATE TABLE IF NOT EXISTS
        users (
            id SERIAL NOT NULL PRIMARY KEY,
            full_name VARCHAR(128),
            email VARCHAR(128) UNIQUE,
            password VARCHAR(128),
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        )`;

        pool.query(queryText).then((res)=>{
            console.log(res);
            pool.end();
        })
        .catch((err)=>{
            console.log(err);
        })
    })
    .catch((error)=>{
        console.log(error);
    })
}
pool.on("remove",()=>{
    console.log("Action done");
})
module.exports = {createUserTable,pool};
require("make-runnable");